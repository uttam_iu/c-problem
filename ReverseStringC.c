#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char str[100];
    printf("Enter a string: ");
    scanf("%[^\n]", &str); //scanning the whole string, including the white spaces

    // printf("%d\n", strlen(str));
    for (int i = strlen(str) - 1; i >= 0; i--)
    {
        printf("%c", str[i]);
    }
}