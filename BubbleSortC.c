#include <stdio.h>

int main(int argc, char const *argv[])
{
    /* code */
    printf("Enter the number of elements: ");
    int n;
    scanf("%d", &n);
    int a[n];
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }

    for (int j = 1; j < n; j++)
    {
        for (int i = 1; i < n; i++)
        {
            if (a[i - 1] > a[i])
            {
                int temp = a[i - 1];
                a[i - 1] = a[i];
                a[i] = temp;
            }
        }
    }

    for (int i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }

    return 0;
}
