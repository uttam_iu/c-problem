/**
 * 
 * insertion sort algorithm
 * 
 * */
#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Enter the number of elements: ");
    int n;
    scanf("%d", &n);
    int arr[n];
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }

    // printf("array length: %d\n", sizeof(arr) / sizeof(int));
    int state = 1;
    for (int i = 0; i < n; i++)
    {
        if (arr[i] > arr[state])
        {
            if (state < n)
            {
                int key = state;
                for (int j = key; j >= 0; j--)
                {
                    if (arr[j] > arr[key])
                    {
                        int temp = arr[j];
                        arr[j] = arr[key];
                        arr[key] = temp;
                        key--;
                    }
                }
            }
        }
        state++;
    }

    for (int i = 0; i < n; i++)
    {
        printf("%d ", arr[i]);
    }

    // printf("this is: %d", a);
    return 0;
}
